<?php

require 'vendor/autoload.php';

use Intervention\Image\ImageManager;

ini_set('max_execution_time', 300);

function writeText($sourceImage, $phrase, $xPosition, $yPosition, $fontSize, $fontPath) {
  for ($i = 0; $i < 2; $i++) {
    if ($i == 0) {
      $color = '#000';
      $offsetXPosition = $xPosition + 2;
      $offsetYPosition = $yPosition + 2;
    } else {
      $color = '#fff';
      $offsetXPosition = $xPosition;
      $offsetYPosition = $yPosition;
    }

    $sourceImage->text($phrase, $offsetXPosition, $offsetYPosition,
      function($font) use ($color, $fontPath, $fontSize) {
        $font->file($fontPath);
        $font->size($fontSize);
        $font->color($color);
        $font->align('left');
        $font->valign('top');
      }
    );
  }
}

if(isset($_POST['name'])){
  $manager = new ImageManager(array('driver' => 'imagick'));

  $name = $_POST['name'];
  $watermark = $manager->make("./img/watermark.png");
  $sourceImage = $manager->make($_FILES['image']['tmp_name']);

  $sourceImage->resize($watermark->width(),null,function($constraint){
    $constraint->aspectRatio();
  });

  $xPosition = 70;
  $yPosition = $sourceImage->height() - $watermark->height() - 100;

  $phrase = strtoupper("$name avec Zinsou");
  $fontPath = './fonts/OpenSans-Bold.ttf';

  writeText($sourceImage, $phrase, $xPosition, $yPosition, 60, $fontPath);

  $phrase = 'LE BÉNIN GAGNANT';
  $fontPath = './fonts/OpenSans-ExtraBold.ttf';

  writeText($sourceImage, $phrase, $xPosition, $yPosition + 72, 76, $fontPath);

  $sourceImage->insert($watermark, 'bottom');

  $path = './save/'.uniqid().$_FILES['image']['name'];
  $sourceImage->save($path);
}

?>

<html>
  <head>
    <title>Image générée</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href='./css/bootstrap/bootstrap.min.css' rel='stylesheet' type='text/css'>
    <script  src="./js/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="./js/bootstrap/bootstrap.min.js" type="text/javascript"></script>

    <link href='./css/app.css' rel='stylesheet' type='text/css'>
  </head>

  <body>
     <div class="jumbotron">
        <h1 class="text-center">AVEC ZINSOU</h1>
        <div align="center">
          <a class="btn btn-transparent" name="button" href="index.php">Recommencer</a>
          <a class="btn btn-transparent" name="button" href="<?php echo $path; ?>">Télécharger</a>
          <button class="btn btn-transparent">Partager sur Facebook</button>
          <button class="btn btn-transparent">Partager sur Twitter</button>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-md-offset-3">
            <div class="thumbnail">
              <img src="<?php echo $path; ?>" alt=""/>
              <div class="caption">

              </div>
            </div>
          </div>
        </div>
      </div>
  </body>
</html>
<script src="./js/app.js" type="text/javascript"></script>
