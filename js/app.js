$(".thumbnail").click(function(){
  $('input[type="file"]').click();
});


$('input[type="file"]').change(function(event){
  var input = event.target, image = document.getElementById('photo');

  var reader = new FileReader();
  reader.onload = function(){
    var dataURL = reader.result;
    image.src = dataURL;
  };

  if(input.files[0]){
    oldFile = input.files[0];
    reader.readAsDataURL(input.files[0]);
  }
  else
    image.src = "img/th.jpg";
});

$("button[type='submit']").click(function(e){
  if($("#photo").src === "img/th.jpg" || $('input[name="name"]').val().trim() === ""){
    e.preventDefault();
  }
})
