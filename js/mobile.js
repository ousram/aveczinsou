var myImage,desc,rec;

var canvas = new fabric.Canvas('mycanvas', {
  backgroundColor: '#fff',
  controlsAboveOverlay:true
});

$("img.ico").click(function(e){
  $(".ico.selected").removeClass('selected');
  $(e.target).addClass('selected');
  loadImage(e.target.src);
})

rec = new fabric.Rect({top:0,left:0,width:295,height:295,fill:'transparent',stroke:"#5B534E",strokeWidth:5});
rec.hasControls = false;
rec.lockMovementX = true;
rec.lockMovementY = true;
rec.lockRotation = true;
canvas.add(rec);



initCanvas();

function initCanvas(){
  rec.set("stroke","#5B534E");
  canvas.remove(desc);
  var descText = "This is X\n\nX types text here\n\nX likes to type text\n\nBe like X";
  desc = new fabric.IText(descText, { left: 20, top: 20,fontSize:15,borderColor:"#5B534E"});
  canvas.add(desc);

  loadImage("/svg/1.svg");

  var watermark = new fabric.Text('www.belikex.com', { left: 180, top: 270,
                                                  fontSize:10,
                                                  fontStyle: 'italic',
                                                  textAlign:"center"});
  watermark.hasControls = false;
  watermark.lockMovementX = true;
  watermark.lockMovementY = true;
  watermark.lockRotation = true;
  canvas.add(watermark);

  canvas.setBackgroundColor("#fff", canvas.renderAll.bind(canvas));
  canvas.setActiveObject(desc);
}


function loadImage(imgUrl){
  canvas.remove(myImage);
  fabric.Image.fromURL(imgUrl, function(oImg) {
    myImage = oImg;
    myImage.set('left',180);
    myImage.set('top',120);
    myImage.set({
      scaleX: 100/myImage.width,
      scaleY: 100/myImage.height
    });
    myImage.set('borderColor',"rgb(100,100,200)");
    canvas.add(myImage);
  });
}

function downloadImage(url) {
  download(url, "BeLikeX.png", "image/png");
}

function uploadImage(){
  canvas.deactivateAll().renderAll();
  document.querySelector("#svg-input").value = canvas.toSVG();
  document.querySelector("#file-input").value = canvas.toDataURL('png');
  document.querySelector("#svg-form").submit();
}


angular.module('belikexapp', ['ionic'])
.controller('RootController', function($scope, $ionicModal) {
    $ionicModal.fromTemplateUrl('img-modal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
    });
    $scope.openModal = function() {
      $scope.modal.show();
    };
    $scope.closeModal = function() {
      $scope.modal.hide();
    };
    //Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function() {
      $scope.modal.remove();
    });
    // Execute action on hide modal
    $scope.$on('modal.hidden', function() {
      // Execute action
    });
    // Execute action on remove modal
    $scope.$on('modal.removed', function() {
      // Execute action
    });
})
