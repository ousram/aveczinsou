<html>
  <head>
    <title>Avec ZINSOU</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href='./css/bootstrap/bootstrap.min.css' rel='stylesheet' type='text/css'>
    <script  src="./js/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="./js/bootstrap/bootstrap.min.js" type="text/javascript"></script>

    <link href='./css/app.css' rel='stylesheet' type='text/css'>
  </head>

  <body>
    <div class="jumbotron">
      <h1 class="text-center">AVEC ZINSOU</h1>
    </div>
    <div class="container">
      <form class="" method="post" action="generate.php"  enctype="multipart/form-data">
        <!-- <meta name="csrf-token" content="{{ csrf_token() }}"> -->
        <div class="row">
          <div class="col-md-4 col-md-offset-4">
            <input type="file" name="image" class="hidden">
            <div class="thumbnail">
              <img src="img/placeholder.png" alt="Cliquez ici pour choisir votre image" id="photo"/>
              <div class="caption">
                <button type="button" name="button" class="form-control btn btn-block btn-default">
                  Choisis ta photo
                </button>
              </div>
            </div>
            <div class="form-group">
              <input type="text" name="name" class="form-control" placeholder="Entre ton nom">
            </div>
            <div class="form-group">
              <button type="submit" name="button" class="btn btn-block btn-small btn-theme form-group">
                Crée ton image "AVEC ZINSOU"
              </button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </body>
</html>
<script src="./js/app.js" type="text/javascript"></script>
